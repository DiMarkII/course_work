#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>
#include <cassert>

using namespace std;

void get_data(vector<char> &first_points, vector<char> &second_points, vector<int> &indices);
vector<char> get_points(vector<char> &first_points, vector<char> &second_points, int &ind_start, int &ind_finish);
vector<vector<int>> get_adj_arr(vector<char> &points, vector<int> &indices, vector<char> &first_points, vector<char> &second_points);
vector<int> find_path(vector<vector<int>> &adj_arr, vector<vector<int>> &residual_arr, int ind1, int ind2);
vector<vector<int>> get_max_stream(vector<vector<int>> &adj_arr, int ind_start, int ind_finish);

int main()
{
    vector<char> first_points, second_points;
    vector<int> indices;
    int ind_start = -1, ind_finish = -1;
    get_data(first_points, second_points, indices);
    vector<char> points = get_points(first_points, second_points, ind_start, ind_finish);
    vector<vector<int>> adj_arr = get_adj_arr(points, indices, first_points, second_points);
    cout << "Adjacency array:\n";
    for(int i = 0; i < adj_arr.size(); ++i)
    {
        for(int j = 0; j < adj_arr[i].size(); ++j)
            cout << (adj_arr[i][j] != INT32_MAX ? adj_arr[i][j] : 0) << ' ';
        cout << '\n';
    }
    vector<vector<int>> max_stream = get_max_stream(adj_arr, ind_start, ind_finish);
    cout << "\nMax flow adjacency array:\n";
    for(int i = 0; i < max_stream.size(); ++i)
    {
        for(int j = 0; j < max_stream[i].size(); ++j)
            cout << (max_stream[i][j] != INT32_MAX ? max_stream[i][j] : 0) << ' ';
        cout << '\n';
    }
    int max_flow = 0;
    for(int i = 0; i < max_stream[ind_start].size(); ++i)
        max_flow += (max_stream[ind_start][i] != INT32_MAX ? max_stream[ind_start][i] : 0);
    cout << "Max flow = " << max_flow << '\n';
    return 0;
}

void get_data(vector<char> &first_points, vector<char> &second_points, vector<int> &indices)
{
    ifstream fin("input.txt");
    while(fin)
    {
        char ch_temp;
        int int_temp;
        fin >> ch_temp;
        if (!fin)
            break;
        first_points.push_back(ch_temp);
        fin >> ch_temp;
        second_points.push_back(ch_temp);
        fin >> int_temp;
        indices.push_back(int_temp);
    }
}

vector<char> get_points(vector<char> &first_points, vector<char> &second_points, int &ind_start, int &ind_finish)
{
    vector<char> result;
    for(int i = 0; i < first_points.size(); ++i)
        if (find(result.begin(), result.end(), first_points[i]) == result.end())
        {
            if (first_points[i] == 'S')
                ind_start = result.size();
            if (first_points[i] == 'T')
                ind_finish = result.size();
            result.push_back(first_points[i]);
        }
    for(int i = 0; i < second_points.size(); ++i)
        if (find(result.begin(), result.end(), second_points[i]) == result.end())
        {
            if (second_points[i] == 'S')
                ind_start = result.size();
            if (second_points[i] == 'T')
                ind_finish = result.size();
            result.push_back(second_points[i]);
        }
    return result;
}

vector<vector<int>> get_adj_arr(vector<char> &points, vector<int> &indices, vector<char> &first_points, vector<char> &second_points)
{
    vector<vector<int>> result;
    for(int i = 0; i < points.size(); ++i)
        result.push_back(vector<int>(points.size(), INT32_MAX));
    for(int i = 0; i < indices.size(); ++i)
    {
        int ind_1 = find(points.begin(), points.end(), first_points[i]) - points.begin();
        int ind_2 = find(points.begin(), points.end(), second_points[i]) - points.begin();
        result[ind_1][ind_2] = indices[i];
    }
    return result;
}

vector<int> find_path(vector<vector<int>> &adj_arr, vector<vector<int>> &residual_arr, int ind1, int ind2)
{
    queue<int> plan;
    vector<bool> is_visited(adj_arr.size());
    vector<int> parent(adj_arr.size(), -1);
    is_visited[ind1] = true;
    parent[ind1] = -1;
    plan.push(ind1);
    bool flag = false;
    while(plan.size() && !flag)
    {
        int ind = plan.front();
        plan.pop();
        for(int i = 0; i < adj_arr[ind].size() && !flag; ++i)
            if (adj_arr[ind][i] != INT32_MAX && adj_arr[ind][i] - residual_arr[ind][i] > 0 && is_visited[i] == false)
            {
                is_visited[i] = true;
                parent[i] = ind;
                plan.push(i);
                if (i == ind2)
                    flag = true;
            }
    }
    vector<int> output;
    if (plan.size() == 0 || is_visited[ind2] == false)
        return output;
    output.push_back(ind2);
    int ind = parent[ind2];
    while(ind != -1)
    {
        output.push_back(ind);
        ind = parent[ind];
    }
    reverse(output.begin(), output.end());
    return output;
}

vector<vector<int>> get_max_stream(vector<vector<int>> &adj_arr, int ind_start, int ind_finish)
{
    vector<vector<int>> result(adj_arr);
    for(int i = 0; i < result.size(); ++i)
        for(int j = 0; j < result[i].size(); ++j)
            if (result[i][j] != INT32_MAX)
                result[i][j] = 0;
    while(true)
    {
        auto path = find_path(adj_arr, result, ind_start, ind_finish);
        if (path.size() == 0)
            return result;
        int min_bandwith = adj_arr[path[0]][path[1]] - result[path[0]][path[1]];
        for(int i = 1; i < path.size() - 1; ++i)
        {
            int now_bandwith = adj_arr[path[i]][path[i + 1]] - result[path[i]][path[i + 1]];
            if (now_bandwith < min_bandwith)
                min_bandwith = now_bandwith;
        }
        for(int i = 0; i < path.size() - 1; ++i)
            result[path[i]][path[i + 1]] += min_bandwith;
    }
}