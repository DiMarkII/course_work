#include "CppUnitTest.h"
#include "main.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
    TEST_CLASS(tests){
    public :
        TEST_METHOD(test)
        {
            vector<vector<int>> adj_arr_1(6, vector<int>(6, INT32_MAX));
            adj_arr_1[0][1] = 3;
            adj_arr_1[0][2] = 3;
            adj_arr_1[1][2] = 2;
            adj_arr_1[1][3] = 3;
            adj_arr_1[2][4] = 2;
            adj_arr_1[3][4] = 4;
            adj_arr_1[3][5] = 2;
            adj_arr_1[4][5] = 3;
            vector<vector<int>> max_stream_1 = get_max_stream(adj_arr_1, 0, 5);
            vector<vector<int>> correct(adj_arr_1);
            correct[0][2] = 2;
            correct[1][2] = 0;
            correct[3][4] = 1;
            Assert::IsTrue(max_stream_1 == correct);
        }
    };
}